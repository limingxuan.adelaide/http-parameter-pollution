'use strict';

// requirements
const express = require('express');
const payment = require('./payment');

// constants
const PORT = process.env.PORT || 8080;
const TRANSFER_ACTION = 'transfer'

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });


app.get('/', (req, res) => {
    if( !req.query.action ||
        typeof req.query.action !== 'string' ||
        req.query.action !== TRANSFER_ACTION ){
        res.status(400).send('You can only transfer an amount');
    }

    const amount = Number(req.query.amount)
    if (Number.isNaN(amount) || amount <= 0 || amount > Number.MAX_SAFE_INTEGER) {
        res.status(400).send('You can only transfer an amount');
    }

    // We only allow transfer
    if(req.query.action === TRANSFER_ACTION) {
        res.send(payment(TRANSFER_ACTION, amount))
        return;
    }
    res.status(400).send('You can only transfer an amount');
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
