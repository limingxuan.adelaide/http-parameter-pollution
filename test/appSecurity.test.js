const request = require('supertest');
const app = require('./app');


describe('security', () => {

    it('Request to transfer and withdraw $100, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer%3Faction=withdraw', amount: '100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to transfer and withdraw $100, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer%26action=withdraw', amount: '100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to invalid action, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: '123', amount: '100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to transfer and withdraw negative amount, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer', amount: '-100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to transfer and withdraw negative amount, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer', amount: '-0'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to transfer and withdraw invalid amount string, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer', amount: 'adfasf'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to withdraw and withdraw amount, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'withdraw', amount: '100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });
});
